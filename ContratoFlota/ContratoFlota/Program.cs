﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace ContratoFlota
{
    class Program
    {

         static void Main()
        {
            ContratoFlota obj = new ContratoFlota();
            obj.clientId = "0348124795";
            obj.contractCode = "string7";
            obj.contractType = null;
            obj.startDate = "2020-06-24";
            obj.endDate = "2020-06-25";
            obj.currentBalance = 0.2;
            obj.extraBalance = 0.3;
            obj.maxBalance = 0.4;
            obj.currentVolume = 0.5;
            obj.maxVolume = 0.6;
            obj.kmControl = true;
            obj.expirationDate = "2020-06-26";
            obj.extendedData = null;
            obj.companyCode = "03481";
            obj.user = "string5";

            //MakeRequest(obj);
            Errores rpt = new Errores();
            rpt.errorsCollection = new List<error>();
            rpt = ContrafoFlota(obj);
            //status = -1 => error | status = 0 => correcto, contractId pk generado
            Console.WriteLine(JsonConvert.SerializeObject(rpt));
            Console.ReadLine();
        }

        static async void MakeRequest(ContratoFlota obj)
        {
            var client = new HttpClient();
            var queryString = HttpUtility.ParseQueryString(string.Empty);

            // Request headers
            client.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", "bd254f6739b5444480919231fde8c4c2");
            client.DefaultRequestHeaders.Add("backend-instance", "3502e53af22d2708396f0cb6b2f1242d");
            string version = "1";
            var uri = "https://apieversaaspre.everilion.com/pre/fleet/contracts/v"+ version + "/contract/create?" + queryString;

            HttpResponseMessage response;

            // Request body
            string jsonString;
            jsonString = JsonConvert.SerializeObject(obj);
            byte[] byteData = Encoding.UTF8.GetBytes(jsonString);

            using (var content = new ByteArrayContent(byteData))
            {
               content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
               response = await client.PostAsync(uri, content);
            }

        }

        private static Errores ContrafoFlota(ContratoFlota obj)
        {
            string version = "1";
            Errores lista = new Errores();
            lista.errorsCollection = new List<error>();
            var url = "https://apieversaaspre.everilion.com/pre/fleet/contracts/v" + version + "/contract/create";
            var request = (HttpWebRequest)WebRequest.Create(url);

            string json = JsonConvert.SerializeObject(obj);
            request.Method = "POST";
            request.ContentType = "application/json";
            request.Accept = "application/json";

            request.Headers.Add("Ocp-Apim-Subscription-Key", "bd254f6739b5444480919231fde8c4c2");
            request.Headers.Add("backend-instance", "3502e53af22d2708396f0cb6b2f1242d");
            using (var streamWriter = new StreamWriter(request.GetRequestStream()))
            {
                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }

            try
            {
                using (WebResponse response = request.GetResponse())
                {
                    using (Stream strReader = response.GetResponseStream())
                    {
                        if (strReader == null) {
                            Errores error = new Errores();
                            error.errorsCollection = new List<error>();
                            error.status = -1;
                            error.errorsCollection.Add(new error { description = "Fallo" });
                        };
                        using (StreamReader objReader = new StreamReader(strReader))
                        {
                            string responseBody = objReader.ReadToEnd();
                            // Do something with responseBody
                            lista = JsonConvert.DeserializeObject<Errores>(responseBody);
                        }
                    }
                }
            }
            catch (WebException ex)
            {
                // Handle error
               
                lista.status = -1;
                lista.errorsCollection.Add(new error { description = ex.Message });
                
                //Console.WriteLine();
            }
            return lista;
        }

    }
}

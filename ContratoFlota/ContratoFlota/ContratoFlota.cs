﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContratoFlota
{
    public class ContratoFlota
    {
        public string clientId { get; set; }
        public string contractCode { get; set; }
        public string contractType { get; set; }

        public string startDate { get; set; }
        public string endDate { get; set; }

        public double currentBalance { get; set; }
        public double extraBalance { get; set; }
        public double maxBalance { get; set; }
        public double currentVolume { get; set; }
        public double maxVolume { get; set; }
        public bool kmControl { get; set; }
        public string expirationDate { get; set; }
        public object extendedData { get; set; }

        public string companyCode { get; set; }
        public string user { get; set; }

    }

    public class error
    {
        public string description { get; set; }
        public string literal {get; set; }
        public int type {get; set; }
    }

    public class Errores {
        public string contractId { get; set; }
        public List<error> errorsCollection { get;set;}
        public int status { get; set; }
    }
}
